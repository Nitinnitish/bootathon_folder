
1.	Students are asked to select the proper components required to rig up the circuit.
2.	Students will click on the connect button to connect the circuit.
3.	Students will be asked to enter the message and carrier signal.
4.	Students will click on CRO visual to generate the graph.
5.	Students will compare the demodulated graph and the message signal graph.
7.	Students will click on Test your Skill tab to test the knowledge they have gained during the experiment.<br>
<b>*Note: If any problem occurs during the experiment the student can refresh the page by clicking on Reset tab.</b>


